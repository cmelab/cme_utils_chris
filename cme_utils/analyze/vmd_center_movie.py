import os

def get_from_traj(TRAJ_FILE, TOP_FILE):
    """
    Uses mdtraj to read the trajectory to find
    the number of frames.
    Requires:
        Trajectory, .dcd file
        Topology, .hoomdxml file
    Returns:
        integer
    """
    import mdtraj as md
    t = md.load(TRAJ_FILE, top = TOP_FILE)
    final_frame = t.n_frames-1 # gets the final frame
    return final_frame

def create_tcl(start_frame, final_frame, step_size, movie_dir):
    """
    Creates the inputs for a tcl script to be called by VMD
    to create snapshots where the center of mass of selected
    particles will be moved to the center of the simulation 
    volume.
    Requires:
        start_frame: integer for the first frame to create a snapshot.
        final_frame: integer for frame of final snapshot
        step_size: integer for the number of frames between snapshots.
        movie_dir: string for directory to save snapshots.
            (NOTE: Because the loop is a while loop it may not actually
            provide the final frame.)
    Returns:
        List of lines to be saved into a tcl script.
    """
    #Reinitialize VMD with the desired settings/selections.
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['mol representation VDW 1.0 8.0\n']
    tclLinesToWrite += ['mol selection "all"\n']
    #### VMD selctions such as: "all" "resid 0" "type C" ####
    tclLinesToWrite += ['mol material AOEdgy\n']
    tclLinesToWrite += ['mol addrep 0\n']
    #Set up loop for creating images of desired frames.
    tclLinesToWrite += ['set number_snapshots 0\n']
    tclLinesToWrite += ['set frame {}\n'.format(start_frame)]
    tclLinesToWrite += ['while {{$frame < {0}}} {{\n'.format(final_frame)]
    tclLinesToWrite += ['animate goto $frame \n']
    tclLinesToWrite += ['pbc box -off \n']
    tclLinesToWrite += 5*['pbc box -color black -centersel "type C" -center com -width 4\n']
    tclLinesToWrite += 5*['pbc wrap -centersel "type C" -center com -now\n'] 
    ### Sometimes takes a few wrappings to get everythin in correcly. ###
    tclLinesToWrite += ['display resetview\n'] 
    #Create image now.
    tclLinesToWrite += ['set sname [format "{}/snapshot-%0.3d.png" $number_snapshots ]\n'.format(movie_dir)] 
    tclLinesToWrite += ['render TachyonInternal vmdscene.tga "convert vmdscene.tga -fuzz 0% -trim -transparent white ./movie/snapshot-$number_snapshots.png"\n'] 
    tclLinesToWrite += ['rm vmdscene.tga\n'] 
    tclLinesToWrite += ['incr number_snapshots 1\n'] 
    tclLinesToWrite += ['incr frame {}\n'.format(step_size)] 
    tclLinesToWrite += ['}\n']
    tclLinesToWrite += ['exit']
    return tclLinesToWrite

def write_tcl_script(tcl_script_name, tclLinesToWrite):
    """
    Write the tcl snapshot loop to a file.
    Requires:
       tcl_script_name : string of filename
       tclLinesToWrite : list of inputs for tcl_script_name
       generated in the create_tcl function.
    Returns: 
        None
    """
    with open(tcl_script_name, 'w+') as tclfile:
        tclfile.writelines(tclLinesToWrite)

def make_movie(start_frame = 1, final_frame = 10,
        step_size = 1, movie_dir = './movie',
        tcl_script_name = 'movie.tcl', 
        TRAJ_FILE = None, TOP_FILE = None):
    """
    Wrapper function to create tcl script.
    Default Values:
        start_frame = 1, 
        final_frame = 10,
        step_size = 1, 
        movie_dir = './movie',
        tcl_script_name = 'movie.tcl', 
        TRAJ_FILE = None, 
        TOP_FILE = None
    Requires:
        None
    Returns:
        None
    """
    if not os.path.exists(movie_dir):
        os.makedirs(movie_dir)
    if TRAJ_FILE != None:
        final_frame = get_from_traj(TRAJ_FILE, TOP_FILE)
    final_frame += 1
    tclLinesToWrite = create_tcl(start_frame, final_frame, step_size, movie_dir)
    write_tcl_script(tcl_script_name, tclLinesToWrite)

if __name__ == "__main__":
    traj = "traj.dcd" #Trajectory file
    top = "restart.hoomdxml" #Topology file
    make_movie(TRAJ_FILE = traj, TOP_FILE = top)
