from PIL import Image
import numpy as np

def rotate_an_combine(images, angles):
    """
    Rotate and average over 
    a series of diffraction images.
    images - list of integers corresponding to 
    the images overwhich to average
        For example: [1, 10, 20] will correspond to
        d0001.png, d0010.png and d0020.png.
    angles - list, angles to rotate (in degrees)
    """

    arr = np.zeros((512, 512), np.float)

    for comb in zip(images, angles):
        image = "{}".format(comb[0]).zfill(4)
        image = "d"+image+".png"
        angle = comb[1]
        print(image, angle)

        img = Image.open(image)
        img = img.rotate(angle)
        pix = np.array(img, dtype=np.float)
        arr=arr+pix/len(images)

    arr = np.array(np.round(arr), dtype=np.uint8)
    out = Image.fromarray(arr)
    out.save('average.png', transparent=True)

if __name__ == "__main__":
    images = [0, 2, 3, 4, 15, 20, 34, 55]
    angles = [17, 22, 17, 10, 24, 3, -27, -9]
