#Generate coordinates for  an FCC crystal. Write out to HOOMD xml?

import numpy

cubic_lattice = numpy.array([[1.,0.,0.],
                             [0.,1.,0.],
                             [0.,0.,1.]])

fcc_basis = numpy.array([[0.0,0.0,0.0],
                         [0.5,0.5,0.0],
                         [0.5,0.0,0.5],
                         [0.0,0.5,0.5]])

bcc_basis = numpy.array([[0.0,0.0,0.0],
                         [0.5,0.5,0.5]])

sc_basis = numpy.array([[0.0,0.0,0]])

def make_crystal(n_repeats, lattice, basis):
    coordinates = []
    for i in range(n_repeats):
        for j in range(n_repeats):
            for k in range(n_repeats):
                translate = i*lattice[0]+j*lattice[1]+k*lattice[2]
                new = basis+translate
                for n in new:
                    coordinates.append(n)
    return numpy.array(coordinates)

print "FCC"
print make_crystal(4,cubic_lattice,fcc_basis)
print "BCC"
print make_crystal(4,cubic_lattice,bcc_basis)
print "SC"
print make_crystal(4,cubic_lattice,sc_basis)






    



    


